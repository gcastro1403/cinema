import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class TheatreTest {
	
	private final Theatre theatre = new Theatre();

	@Test
	void testIsValidNameTrue() {
		
		boolean result = theatre.isValidName("A8");
		assertTrue(result);
		
	}
	@Test
	void testIsValidNameFalse() {
		boolean result = theatre.isValidName("00");
		assertFalse(result);
	}
	
	@Test
	void testgenerateColAndRow() {
		List<String> subList = theatre.generateColAndRow(4);
		List<String> shouldBeAResult = new ArrayList<>(Arrays.asList("A","B","C","D"));
		assertEquals(subList,shouldBeAResult);
		
	}
	
	@Test
	void testgenerateSeats() {
		theatre.numberOfRows = 4;
		List<String> lettersOfSeats =  new ArrayList<>(Arrays.asList("A","B"));
		theatre.generateSeats(lettersOfSeats, 2);
		assertEquals(theatre.numberOfSeats, 4);
	}
	
	@Test 
	void testreserveSeats() {
		theatre.numberOfRows = 4;
		List<String> lettersOfSeats =  new ArrayList<>(Arrays.asList("A","B"));
		theatre.generateSeats(lettersOfSeats, 2);
		theatre.cancelOrReserve("A2", true);
		theatre.reserveSeats("A2");
		boolean isReserved = theatre.generatedRow.get(1).getReserveSeat();
		assertTrue(isReserved);
	}
	
	@Test 
	void testcancelSeats() {
		theatre.numberOfRows = 4;
		List<String> lettersOfSeats =  new ArrayList<>(Arrays.asList("A","B"));
		theatre.generateSeats(lettersOfSeats, 2);
		theatre.cancelSeats("A2");
		boolean isReserved = theatre.generatedRow.get(1).getReserveSeat();
		assertFalse(isReserved);
	}
	
	@Test
	void testprintSeats() {
		theatre.numberOfRows = 4;
		List<String> lettersOfSeats =  new ArrayList<>(Arrays.asList("A","B"));
		theatre.generateSeats(lettersOfSeats, 2);
		theatre.printSeats();
	}
	
	

}
