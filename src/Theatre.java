import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Theatre {
	
	String nameTheater = "Java Cinema";
	Integer numberOfRows;
	Integer numberOfSeats;
	List<Seat> generatedRow = new ArrayList<>();
	
	List<String> lettersList = new ArrayList<>(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N",
															 "O","P","Q","R","S","T","U","V","W","X","Y","Z"));
	
	
	public List<String> generateColAndRow (int numberCols) {
		List<String> colsTotal = this.lettersList.subList(0, numberCols);
		this.numberOfRows = numberCols;
		this.generateSeats(colsTotal, numberCols);
		return colsTotal; 
	}
	
	public void generateSeats (List<String> rowsOfSeats, int numberCols) {
		rowsOfSeats.forEach( element -> {
			for(int i = 0; i<= numberCols - 1; i++) {
				Seat seatPerRow = new Seat();
				seatPerRow.setLineSeat(i + 1);
				seatPerRow.setRowSeat(element);
				this.generatedRow.add(seatPerRow);
			}
		});
		this.numberOfSeats = this.generatedRow.size();
		//this.cancelSeats("A2");
		this.reserveSeats("A5");
		this.reserveSeats("F5");
		this.reserveSeats("H5");
		this.reserveSeats("D5");
		this.printSeats();
		
	}
	
	public boolean isValidName (String seatName) {
		Pattern regex = Pattern.compile("^[A-Z]\\d");
		Matcher foundIt = regex.matcher(seatName);
		return foundIt.matches() ? true :false;
	}
	
	public void  reserveSeats ( String seatsToReserve ) {
		
		if(this.isValidName(seatsToReserve)) {
			
			cancelOrReserve(seatsToReserve, true);
		}else {
			
			System.out.println("Not Found");
		}
		
	}
	
	
	public void cancelSeats( String seatToCancell ) {
		
		cancelOrReserve(seatToCancell, false);
	}
	
	
	public void cancelOrReserve(String seatToUpdate, Boolean action) {
		
			String rowCancel = seatToUpdate.substring(0,1);
			String  strLineCancel = seatToUpdate.substring(1);
			Integer numLineCancel = Integer.parseInt(strLineCancel);

			 this.generatedRow.stream()
			.filter(current -> rowCancel.equals(current.getRowSeat()) && numLineCancel.equals(current.getLineSeat()))
			.map( current -> {
				current.setReserveSeat(action);
				return current;
			})
			.collect(Collectors.toList());
	
	}
	

	public void printSeats () {
		List<String> getSameRow = new ArrayList<String>();
		List<Seat> seatSold =  new ArrayList<Seat>();
		this.generatedRow.forEach(element ->{
			if(element.getReserveSeat()) {
				getSameRow.add(".");
				seatSold.add(element);
			}else {
				getSameRow.add(element.toString());				
			}
		});
		String rowsString = Integer.toString(numberOfRows);
		String str = String.join(" ",getSameRow);
		String strwithReplace = str.replace(rowsString, rowsString+"\n");
		
		Integer totalIncomes = seatSold.size() * 50;
		Integer seatsAvailables = this.numberOfSeats - seatSold.size();
				
		System.out.print(this.nameTheater+ "\n");
		System.out.println(strwithReplace);
		System.out.println("Total available:" + seatsAvailables);
		System.out.println("Total sold: "+ seatSold.size());
		System.out.println("Total Income: " + totalIncomes);
	}
	
	
	
	
	
	
	

}
