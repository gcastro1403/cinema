
public class Seat {
	
	private String rowSeat;
	
	private Integer lineSeat;
	
	private Integer priceSeat;
	
	private Boolean reserveSeat = false;
	
	public Boolean getReserveSeat() {
		return reserveSeat;
	}
	public void setReserveSeat(Boolean reserveSeat) {
		this.reserveSeat = reserveSeat;
	}
	public String getRowSeat() {
		return rowSeat;
	}
	public void setRowSeat(String rowSeat) {
		this.rowSeat = rowSeat;
	}
	public Integer getLineSeat() {
		return lineSeat;
	}
	public void setLineSeat(Integer lineSeat) {
		this.lineSeat = lineSeat;
	}
	public Integer getPriceSeat() {
		return priceSeat;
	}
	public void setPriceSeat(Integer priceSeat) {
		this.priceSeat = priceSeat;
	}
	
	@Override
    public String toString() {
        return this.rowSeat + this.lineSeat  ;
    }
	
	
}
